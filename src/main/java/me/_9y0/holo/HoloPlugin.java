package me._9y0.holo;

import me._9y0.holo.commands.HoloCommand;
import me._9y0.holo.listeners.PlayerJoinListener;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class HoloPlugin extends JavaPlugin {

	public static final String PREFIX = "&e&lHolo&7 &b&l>> &r";

	private static HoloPlugin instance;

	// TODO: https://bukkit.org/threads/support-multiple-minecraft-versions-with-abstraction-maven.115810/

	public static HoloPlugin getInstance() {
		return instance;
	}

	@Override
	public void onEnable() {
		instance = this;

		getCommand("holo").setExecutor(new HoloCommand());

		registerListeners();
	}

	private void registerListeners() {
		Bukkit.getPluginManager().registerEvents(new PlayerJoinListener(), this);
	}

	@Override
	public void onDisable() {
		instance = null;
	}
}
