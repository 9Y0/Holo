package me._9y0.holo.commands;

import java.util.ArrayList;
import java.util.Arrays;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import me._9y0.holo.user.HoloUser;
import me._9y0.holo.util.InfoUtil;

public class HoloCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		HoloUser user = new HoloUser(sender);
		if (args.length == 0) {
			user.sendPrefixedMessage(InfoUtil.getInfo());
			user.sendPrefixedMessage("&7Type &b/holo help &7to see all commands.");
			return true;
		}

		SubCommand subCommand = SubCommandManager.getInstance().search(args[0]);
		if (subCommand == null) {
			user.sendPrefixedMessage("&cUnknown command.");
			user.sendPrefixedMessage("&7Type &b/holo help &7to see all commands.");
			return true;
		}

		if (subCommand.isPlayerOnly() && !user.isPlayer()) {
			user.sendPrefixedMessage("&cOnly players can excecute this command.");
			return true;
		}

		if (!user.getSender().hasPermission(subCommand.getPermission())) {
			user.sendPrefixedMessage("&cYou don't have permssions to excecute this command.");
			return true;
		}

		ArrayList<String> argsList = new ArrayList<>(Arrays.asList(args));
		argsList.remove(0);
		subCommand.execute(user, argsList.toArray(new String[argsList.size()]));
		return true;
	}

}
