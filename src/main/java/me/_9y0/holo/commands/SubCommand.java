package me._9y0.holo.commands;

import me._9y0.holo.user.HoloUser;

public abstract class SubCommand {

	private String name;
	private String[] aliases;
	private String description;
	private String permission;
	private String usage;
	private boolean playerOnly;

	public SubCommand(String name, String description, String permission, String usage, boolean playerOnly,
			String... aliases) {
		this.name = name;
		this.aliases = aliases;
		this.description = description;
		this.permission = permission;
		this.usage = usage;
		this.playerOnly = playerOnly;
	}

	public abstract void execute(HoloUser user, String[] args);

	public String getName() {
		return name;
	}

	public String[] getAliases() {
		return aliases;
	}

	public String getDescription() {
		return description;
	}

	public String getPermission() {
		return permission;
	}

	public String getUsage() {
		return usage;
	}

	public boolean isPlayerOnly() {
		return playerOnly;
	}

}
