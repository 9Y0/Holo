package me._9y0.holo.commands;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import me._9y0.holo.commands.subcommands.*;

public class SubCommandManager {

	public final static int COMMANDS_PER_PAGE = 5;
	private static final SubCommandManager instance = new SubCommandManager();
	private final List<SubCommand> commands = new ArrayList<>();

	private SubCommandManager() {
		// TODO: Add subcommands
		this.commands.add(new HelpSubCommand());
		this.commands.add(new CreateSubCommand());
		this.commands.add(new RemoveSubCommand());
		this.commands.add(new ListSubCommand());
		this.commands.add(new EditSubCommand());
	}

	public static SubCommandManager getInstance() {
		return instance;
	}

	public SubCommand search(String name) {
		for (SubCommand subCommand : commands) {
			if (subCommand.getName().equalsIgnoreCase(name)
					|| Arrays.stream(subCommand.getAliases()).anyMatch(string -> string.equalsIgnoreCase(name)))
				return subCommand;
		}
		return null;
	}

	public List<SubCommand> getPage(int page) {
		// If input is 1, we want to use 0.
		page--;

		int begin = page * COMMANDS_PER_PAGE;
		int end = page * COMMANDS_PER_PAGE + COMMANDS_PER_PAGE;

		if (end > commands.size())
			end = commands.size();

		return commands.subList(begin, end);
	}

	public List<SubCommand> getCommands() {
		return commands;
	}
}
