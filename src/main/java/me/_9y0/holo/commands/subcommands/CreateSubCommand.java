package me._9y0.holo.commands.subcommands;

import me._9y0.holo.commands.SubCommand;
import me._9y0.holo.user.HoloUser;

public class CreateSubCommand extends SubCommand {

	public CreateSubCommand() {
		super("create", "Create a new hologram.", "holo.create", "<name> [content]", true, "cr", "make");
	}

	@Override
	public void execute(HoloUser user, String[] args) {
		// TODO
	}
}
