package me._9y0.holo.commands.subcommands;

import java.util.List;

import me._9y0.holo.commands.SubCommandManager;
import me._9y0.holo.commands.SubCommand;
import me._9y0.holo.user.HoloUser;
import me._9y0.holo.util.StringUtil;

public class HelpSubCommand extends SubCommand {

	public HelpSubCommand() {
		super("help", "Show the help page.", "holo.viewhelp", "[page]", false, "h");
	}

	@Override
	public void execute(HoloUser user, String[] args) {
		int page = 1;
		if (args.length != 0) {
			if (StringUtil.isInteger(args[0])) {
				page = Integer.parseInt(args[0]);
				if (page < 1)
					page = 1;
			}
		}

		String typeForNextPage;
		if ((page - 1) * SubCommandManager.COMMANDS_PER_PAGE > SubCommandManager.getInstance().getCommands().size()) {
			page = SubCommandManager.getInstance().getCommands().size() / SubCommandManager.COMMANDS_PER_PAGE + 1;
			typeForNextPage = "&7This is the last help page.";
		} else
			typeForNextPage = "&7Type &b/holo help " + (page + 1) + " &7to see the next page.";

		List<SubCommand> subCommands = SubCommandManager.getInstance().getPage(page);

		user.sendMessage("&e&lHolo &7help &b(page " + page + ")&7. " + typeForNextPage);
		for (SubCommand subCommand : subCommands) {
			String usage = subCommand.getUsage().isEmpty() ? "" : " " + subCommand.getUsage();
			user.sendMessage("&e/holo &b" + subCommand.getName() + usage + " &7- " + subCommand.getDescription());
		}
	}
}
