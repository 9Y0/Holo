package me._9y0.holo.commands.subcommands;

import me._9y0.holo.commands.SubCommand;
import me._9y0.holo.user.HoloUser;

public class ListSubCommand extends SubCommand {

	public ListSubCommand() {
		super("list", "See a list of all holograms.", "holo.list", "[page]", false, "ls");
	}

	@Override
	public void execute(HoloUser user, String[] args) {
		// TODO
	}

}
