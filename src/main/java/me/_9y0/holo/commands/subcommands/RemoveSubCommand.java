package me._9y0.holo.commands.subcommands;

import me._9y0.holo.commands.SubCommand;
import me._9y0.holo.user.HoloUser;

public class RemoveSubCommand extends SubCommand {

	public RemoveSubCommand() {
		super("remove", "Remove an existing hologram.", "holo.remove", "<name>", false, "rm", "delete");
	}

	@Override
	public void execute(HoloUser user, String[] args) {
		// TODO
	}

}
