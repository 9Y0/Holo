package me._9y0.holo.hologram;

import java.util.HashSet;
import java.util.Set;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.ArmorStand;

public class Hologram {

	private final Set<ArmorStand> entities = new HashSet<>();
	//private final Location location;
	private VisibilityManager visibilityManager;

	/**
	 * This constructor is used to load a hologram from config.
	 *
	 * @param section
	 *            The section where the info is stored.
	 */
	public Hologram(ConfigurationSection section) {

	}

	public VisibilityManager getVisibilityManager() {
		return visibilityManager;
	}
}
