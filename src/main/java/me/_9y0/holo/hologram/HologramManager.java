package me._9y0.holo.hologram;

import java.util.HashSet;
import java.util.Set;

public class HologramManager {

	private static final HologramManager instance = new HologramManager();

	private final Set<Hologram> holograms = new HashSet<>();

	private HologramManager() {
	}

	public static HologramManager getInstance() {
		return instance;
	}

    public Set<Hologram> getHolograms() {
		return holograms;
    }
}
