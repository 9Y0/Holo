package me._9y0.holo.hologram;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class VisibilityManager {

	private final Set<UUID> visibleTo = new HashSet<>();

	private boolean visibleByDefault = true;

	public void showTo(Player player) {

	}

	public void hideFor(Player player) {
        if (!isVisibleTo(player)) return;


	}

	public void showDefault(Player player) {
		if (visibleByDefault)
			showTo(player);
		else
			hideFor(player);
	}

	public boolean isVisibleByDefault() {
		return visibleByDefault;
	}

	public void setVisibleByDefault(boolean visibleByDefault) {
		if (this.visibleByDefault = visibleByDefault)
			return;

		this.visibleByDefault = visibleByDefault;
		if (visibleByDefault)
			Bukkit.getOnlinePlayers().forEach(this::showTo);
		else
			Bukkit.getOnlinePlayers().stream().filter(this::isVisibleTo).forEach(this::hideFor);
	}

	public boolean isVisibleTo(Player player) {
		return visibleTo.contains(player.getUniqueId());
	}

}
