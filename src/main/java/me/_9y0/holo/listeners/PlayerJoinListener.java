package me._9y0.holo.listeners;

import me._9y0.holo.hologram.HologramManager;
import me._9y0.holo.hologram.Hologram;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerJoinListener implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        for (Hologram hologram : HologramManager.getInstance().getHolograms()) {
            hologram.getVisibilityManager().showDefault(event.getPlayer());
        }
    }

}
