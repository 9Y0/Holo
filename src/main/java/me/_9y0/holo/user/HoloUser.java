package me._9y0.holo.user;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me._9y0.holo.HoloPlugin;
import me._9y0.holo.util.ChatUtil;

public class HoloUser {

	private final CommandSender sender;

	public HoloUser(CommandSender sender) {
		this.sender = sender;
	}

	public void sendPrefixedMessage(String message) {
		sendMessage(HoloPlugin.PREFIX + message);
	}

	public void sendMessage(String message) {
		sender.sendMessage(ChatUtil.color(message));
	}

	public boolean isPlayer() {
		return sender instanceof Player;
	}

	public CommandSender getSender() {
		return sender;
	}

	public Player getPlayer() {
		return (Player) sender;
	}
}
