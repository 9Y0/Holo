package me._9y0.holo.util;

import me._9y0.holo.HoloPlugin;

public class InfoUtil {

	public static String getInfo() {
		return "&7Running &e&l" + HoloPlugin.getInstance().getDescription().getName() + " &7v"
				+ HoloPlugin.getInstance().getDescription().getVersion() + " by &b"
				+ StringUtil.formatList(HoloPlugin.getInstance().getDescription().getAuthors(), "&7, &b");
	}
}
