package me._9y0.holo.util;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.ConfigurationSection;

public class LocationUtil {

	/**
	 * <b>NOTE:</b> You need to save the config after this.
	 */
	public static void saveLocation(Location location, ConfigurationSection section) {
		section.set("world", location.getWorld());
		section.set("x", location.getX());
		section.set("y", location.getY());
		section.set("z", location.getZ());
		section.set("yaw", location.getYaw());
		section.set("pitch", location.getPitch());
	}

	public static Location loadLocation(ConfigurationSection section) {
		World world = Bukkit.getWorld(section.getString("world"));
		if (world == null) {
			throw new IllegalArgumentException("Unknown world!");
		}

		return new Location(world, section.getInt("x"), section.getInt("y"), section.getInt("z"), section.getInt("yaw"), section.getInt("pitch"));
	}

}
