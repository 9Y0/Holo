package me._9y0.holo.util;

import java.util.Iterator;
import java.util.List;

public class StringUtil {

	public static String formatList(List<String> list, String separator) {
		StringBuilder builder = new StringBuilder();
		Iterator<String> iterator = list.iterator();

		while (iterator.hasNext()) {
			builder.append(iterator.next());
			if (iterator.hasNext())
				builder.append(separator);
		}

		return builder.toString().trim();
	}

	public static boolean isInteger(String string) {
		try {
			Integer.parseInt(string);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
}
